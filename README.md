# Spring Boot Vue

A demo of my recent tech stack with Vuejs and spring boot and docker-compose 

## Technologies

* [Spring Boot](http://projects.spring.io/spring-boot/)
* [Maven](http://maven.apache.org/)
* [Spring MVC REST](http://spring.io/guides/gs/rest-service/)
* [Spring Data JPA](http://projects.spring.io/spring-data-jpa/)
* [MySQL](https://www.mysql.com) (Production, Development)
* [Flyway Database Migration](http://flywaydb.org/)
* [Docker](https://www.docker.com/)
* [Docker Compose](https://docs.docker.com/compose/)
* [VueJs](https://vuejs.org)
* ...

## Deploy

```
# Build the project
mvn package

# build the images
docker-compose build --no-cache

# running
docker-compose up

```

## License

Released under [the MIT license](LICENSE).
