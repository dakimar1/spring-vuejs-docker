--
-- Application tables
--

create table `users`
(
  `id`             int         not null auto_increment,
  `email`          varchar(60) null unique,
  `email_verified` integer(1)  not null default 0,
  `password`       text        null,
  `name`           varchar(60) null,
  `last_name`      varchar(60) null,
  `first_name`     varchar(60) null,
  `provider`       varchar(32) not null,
  `user_type`      varchar(32) not null,
  `id_provider`    text        null,
  `creation`       datetime    not null,
  `last_update`    datetime    null,
  primary key (
               `id`
    )
);



